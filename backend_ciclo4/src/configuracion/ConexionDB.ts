import { connect } from "mongoose";

const ConexionDB = () => {
  const urlConexion = String(process.env.DB_MONGO);
  connect(urlConexion)
    .then(()=>{
        console.log("conexion estabkecida:", process.env.DB_MONGO);
    })
    .catch((miError) => {
      console.log("error al conectarse a la base", miError);
    });
};

export default ConexionDB;
