import PerfilEntidad from './PerfilEntidad';
export class UsuarioEntidad {
  public nombreUsuario: string;
  public correoUsuario: string;
  public claveUsuario: string;
  public estadoUsuario: number;
  public fechaCreacionUsuario: Date;
  public codPerfil: PerfilEntidad;

  constructor(nomp: string, esta: number, corr: string, usua: string, fech: Date, codp: PerfilEntidad) {
    this.nombreUsuario = nomp;
    this.estadoUsuario = esta;
    this.correoUsuario = corr;
    this.claveUsuario = usua;
    this.fechaCreacionUsuario = fech;
    this.codPerfil = codp;



  }
}

export default UsuarioEntidad;
