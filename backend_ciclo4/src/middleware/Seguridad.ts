import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";

class Seguridad {

  public verificar(req:Request, res:Response, next: NextFunction) {
    if (req.headers.authorization) {
       try {
        const miLlavePrivada = String (process.env.SECRETA);
        const tokenRecibido = req.headers.authorization?.split(" ")[1] as string;
        const laInformacion = jwt.verify(tokenRecibido, miLlavePrivada);
        req.body.datosUsuario = laInformacion;
        next();
       } catch (error) {
        res.status(401).json({respuesta: "pal cai por falsi"})
       } 
    } else {
        res.status(401).json({respuesta: "patada al ojo"})
    }
  }
}

const seguridad = new Seguridad();
export default seguridad;
