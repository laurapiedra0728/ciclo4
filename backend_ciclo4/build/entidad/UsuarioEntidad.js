"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsuarioEntidad = void 0;
class UsuarioEntidad {
    constructor(nomp, esta, corr, usua, fech, codp) {
        this.nombreUsuario = nomp;
        this.estadoUsuario = esta;
        this.correoUsuario = corr;
        this.claveUsuario = usua;
        this.fechaCreacionUsuario = fech;
        this.codPerfil = codp;
    }
}
exports.UsuarioEntidad = UsuarioEntidad;
exports.default = UsuarioEntidad;
