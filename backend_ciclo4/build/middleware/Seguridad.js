"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
class Seguridad {
    verificar(req, res, next) {
        var _a;
        if (req.headers.authorization) {
            try {
                const miLlavePrivada = String(process.env.SECRETA);
                const tokenRecibido = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(" ")[1];
                const laInformacion = jsonwebtoken_1.default.verify(tokenRecibido, miLlavePrivada);
                req.body.datosUsuario = laInformacion;
                next();
            }
            catch (error) {
                res.status(401).json({ respuesta: "pal cai por falsi" });
            }
        }
        else {
            res.status(401).json({ respuesta: "patada al ojo" });
        }
    }
}
const seguridad = new Seguridad();
exports.default = seguridad;
